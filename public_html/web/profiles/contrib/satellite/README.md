# Satellite 5 Profile

This is the Drupal profile of the Satellite 5 distribution by the [IT Committee](https://it.esn.org) of [ESN International](https://esn.org).

### Codesniffer setup
Since we are using ddev, we suppose PHP is not installed on the host machine. In order to make it DDEV compatible follow the instructions below
```
cd $(git rev-parse --show-cdup) # Go to project root, where .git folder is
chmod +x assets/git/pre-commit
cd .git/hooks && ln -s ../../assets/git/pre-commit
```
