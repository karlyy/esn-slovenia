<?php

namespace Drupal\satellite\Exception;

/**
 * @file
 * Contains the module configuration form from the
 * {@link https://www.drupal.org/project/social Open Social distribution}.
 *
 * The code has been adapted by the
 * {@link https://it.esn.org IT Committee of ESN International} to the
 * requirements of the ESN Satellite distribution.
 *
 * @author {@link https://www.drupal.org/open-social Open Social}
 * @copyright 2021 Open Social
 * @license https://spdx.org/licenses/GPL-2.0-or-later.html
 */

/**
 * An exception thrown by the installer for invalid optional feature data.
 */
class OptionalFeatureDataException extends \RuntimeException {
}
