<?php

namespace Drupal\satellite\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Satellite home routes.
 */
class SatelliteHomeController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    return [];
  }

}
