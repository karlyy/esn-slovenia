<?php

namespace Drupal\satellite_demo\Controller;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Core\Controller\ControllerBase;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Returns responses for Satellite demo routes.
 */
class SatelliteDemoController extends ControllerBase {

  /**
   * Run the node_migration_no_migrate_drupal test migration.
   *
   * @return array
   *   A renderable array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function execute() {
    /** @var \Drupal\migrate\Plugin\MigrationPluginManager $migration_plugin_manager */
    $migration_plugin_manager = \Drupal::service('plugin.manager.migration');
    $definitions = $migration_plugin_manager->getDefinitions();
    $demo_migration_ids = ['menus', 'paragraphs', 'blocks', 'files'];
    foreach ($demo_migration_ids as $demo_migration_id) {
      if (!isset($definitions[$demo_migration_id])) {
        throw new InvalidPluginDefinitionException($demo_migration_id);
      }
    }

    $migrations = $migration_plugin_manager->createInstancesByTag('Satellite Demo');
    foreach ($migrations as $migration) {
      $result = (new MigrateExecutable($migration))->import();
      if ($result !== MigrationInterface::RESULT_COMPLETED) {
        throw new \RuntimeException('Migration failed');
      }
    }

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('Migration successful!'),
    ];
    $this->state()->set('satellite_demo_migrations_have_run', 1);

    return $build;
  }

}
