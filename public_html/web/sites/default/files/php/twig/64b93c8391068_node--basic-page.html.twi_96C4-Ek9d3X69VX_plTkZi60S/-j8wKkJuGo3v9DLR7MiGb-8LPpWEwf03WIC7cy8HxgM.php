<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* profiles/contrib/satellite/themes/satellite_theme/templates/content/node--basic-page.html.twig */
class __TwigTemplate_0fab850990ea414f914a1d6075d274aab657ce4c10f8ba655ec4913c4c7710fb extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doGetParent(array $context)
    {
        // line 75
        return "@satellite_theme/node/node.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@satellite_theme/node/node.twig", "profiles/contrib/satellite/themes/satellite_theme/templates/content/node--basic-page.html.twig", 75);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 76
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 77
        echo "  <h2 class=\"display-6 fw-bold text-esn-cyan\">";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null), 77, $this->source), "html", null, true);
        echo "</h2>
  <p class=\"display-8\">";
        // line 78
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["date"] ?? null), 78, $this->source), "html", null, true);
        echo "</p>
  <p class=\"lead\">";
        // line 79
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, twig_striptags($this->sandbox->ensureToStringAllowed((($__internal_compile_0 = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_subtitle", [], "any", false, false, true, 79), 0, [], "any", false, false, true, 79)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0["#text"] ?? null) : null), 79, $this->source)), "html", null, true);
        echo "</p>
";
        // line 81
        echo "  &nbsp;
  &nbsp;
  &nbsp;
  &nbsp;
  ";
        // line 85
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->withoutFilter($this->sandbox->ensureToStringAllowed(($context["content"] ?? null), 85, $this->source), "field_subtitle"), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "profiles/contrib/satellite/themes/satellite_theme/templates/content/node--basic-page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 85,  65 => 81,  61 => 79,  57 => 78,  52 => 77,  48 => 76,  37 => 75,);
    }

    public function getSourceContext()
    {
        return new Source("", "profiles/contrib/satellite/themes/satellite_theme/templates/content/node--basic-page.html.twig", "C:\\Users\\Karolina\\Desktop\\esn\\public_html\\web\\profiles\\contrib\\satellite\\themes\\satellite_theme\\templates\\content\\node--basic-page.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array();
        static $filters = array("escape" => 77, "striptags" => 79, "without" => 85);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape', 'striptags', 'without'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
