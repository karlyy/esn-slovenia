<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* profiles/contrib/satellite/themes/satellite_theme/templates/page/page.html.twig */
class __TwigTemplate_06444da3e72c530d03d3fd7c9af1dc0532c832edc7718f7d45130791a99fca98 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 48
        echo "<div class=\"page\">
  ";
        // line 49
        $this->loadTemplate("profiles/contrib/satellite/themes/satellite_theme/templates/page/page.html.twig", "profiles/contrib/satellite/themes/satellite_theme/templates/page/page.html.twig", 49, "225828807")->display(twig_array_merge($context, ["placement" => (( !        // line 50
($context["logged_in"] ?? null)) ? ("sticky-top") : ("")), "container" => "fixed", "color" => "light", "utility_classes" => [0 => "bg-light"]]));
        // line 76
        echo "
  <main class=\"pt-5 pb-5\">
    <a id=\"main-content\" tabindex=\"-1\"></a>";
        // line 79
        echo "
    ";
        // line 80
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "header", [], "any", false, false, true, 80)) {
            // line 81
            echo "      <header class=\"page__header mb-3\">
        <div class=\"container\">
          ";
            // line 83
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "header", [], "any", false, false, true, 83), 83, $this->source), "html", null, true);
            echo "
        </div>
      </header>
    ";
        }
        // line 87
        echo "
    ";
        // line 88
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "content", [], "any", false, false, true, 88)) {
            // line 89
            echo "      <div class=\"page__content\">
        <div class=\"container\">
          ";
            // line 91
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "content", [], "any", false, false, true, 91), 91, $this->source), "html", null, true);
            echo "
        </div>
      </div>
    ";
        }
        // line 95
        echo "  </main>

  <hr>

  ";
        // line 100
        echo "
  <footer class=\"container pt-2 pt-md-3\">
    <div class=\"row\">

      <div class=\"col-md-2\">
        <ul class=\"list-unstyled text-small\">

          <li class=\"mb-1\"><a class=\"link-secondary text-decoration-none\" href=\"";
        // line 107
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["front_page"] ?? null), 107, $this->source), "html", null, true);
        echo "\"><img
                  src=\"";
        // line 108
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["esn_data"] ?? null), "logo_url", [], "any", false, false, true, 108), 108, $this->source), "html", null, true);
        echo "\" alt=\"website logo\" width=\"100%\"/></a>
          </li>

        </ul>
      </div>
      <div class=\"col-md-5\">

        <ul class=\"list-unstyled text-small\">
          <li class=\"mb-1\"><a class=\"link-secondary text-decoration-none fw-bold\" href=\"#\">";
        // line 116
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["site_name"] ?? null), 116, $this->source), "html", null, true);
        echo "</a></li>
          <li class=\"mb-1 text-decoration-none footer-color-grey\">";
        // line 117
        echo twig_nl2br($this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["esn_data"] ?? null), "address", [], "any", false, false, true, 117), 117, $this->source), "html", null, true));
        echo "</li>
";
        // line 119
        echo "          <li class=\"mb-1\"><a class=\"link-secondary text-decoration-none\" href=\"#\">";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["esn_data"] ?? null), "email", [], "any", false, false, true, 119), 119, $this->source), "html", null, true);
        echo "</a></li>
        </ul>
      </div>
      <div class=\"col-md-3  d-flex flex-column justify-content-start align-items-end\">

        <ul class=\"list-unstyled text-small\">
          <li class=\"mb-1\"><a class=\"link-secondary text-decoration-none\" href=\"https://www.esn.org/\" target=\"_blank\">ESN International</a></li>
          ";
        // line 126
        if ((twig_get_attribute($this->env, $this->source, ($context["esn_data"] ?? null), "level", [], "any", false, false, true, 126) === "local")) {
            // line 127
            echo "          <li class=\"mb-1\"><a class=\"link-secondary text-decoration-none\" href=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["esn_data"] ?? null), "country_site", [], "any", false, false, true, 127), 127, $this->source), "html", null, true);
            echo "\" target=\"_blank\">";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["esn_data"] ?? null), "country_name", [], "any", false, false, true, 127), 127, $this->source), "html", null, true);
            echo "</a></li>
          ";
        }
        // line 129
        echo "          <li class=\"mb-1\"><a class=\"link-secondary text-decoration-none\" href=\"https://www.esncard.org\" target=\"_blank\">ESNcard.org</a></li>
        </ul>
      </div>
      <div class=\"col-md-2 d-flex flex-column justify-content-start align-items-end\">
        <ul class=\"list-unstyled text-small\">
          ";
        // line 134
        if (twig_get_attribute($this->env, $this->source, ($context["esn_data"] ?? null), "facebook", [], "any", false, false, true, 134)) {
            // line 135
            echo "          <li class=\"mb-1\"><a class=\"link-secondary text-decoration-none fw-bold\" href=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["esn_data"] ?? null), "facebook", [], "any", false, false, true, 135), 135, $this->source), "html", null, true);
            echo "\" target=\"_blank\">Facebook</a></li>
          ";
        }
        // line 137
        echo "          ";
        if (twig_get_attribute($this->env, $this->source, ($context["esn_data"] ?? null), "twitter", [], "any", false, false, true, 137)) {
            // line 138
            echo "          <li class=\"mb-1\"><a class=\"link-secondary text-decoration-none fw-bold\" href=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["esn_data"] ?? null), "twitter", [], "any", false, false, true, 138), 138, $this->source), "html", null, true);
            echo "\" target=\"_blank\">Twitter</a></li>
          ";
        }
        // line 140
        echo "          ";
        if (twig_get_attribute($this->env, $this->source, ($context["esn_data"] ?? null), "instagram", [], "any", false, false, true, 140)) {
            // line 141
            echo "          <li class=\"mb-1\"><a class=\"link-secondary text-decoration-none fw-bold\" href=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["esn_data"] ?? null), "instagram", [], "any", false, false, true, 141), 141, $this->source), "html", null, true);
            echo "\" target=\"_blank\">Instagram</a></li>
          ";
        }
        // line 143
        echo "        </ul>
      </div>


    </div>
  </footer>
</div>
";
    }

    public function getTemplateName()
    {
        return "profiles/contrib/satellite/themes/satellite_theme/templates/page/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  173 => 143,  167 => 141,  164 => 140,  158 => 138,  155 => 137,  149 => 135,  147 => 134,  140 => 129,  132 => 127,  130 => 126,  119 => 119,  115 => 117,  111 => 116,  100 => 108,  96 => 107,  87 => 100,  81 => 95,  74 => 91,  70 => 89,  68 => 88,  65 => 87,  58 => 83,  54 => 81,  52 => 80,  49 => 79,  45 => 76,  43 => 50,  42 => 49,  39 => 48,);
    }

    public function getSourceContext()
    {
        return new Source("", "profiles/contrib/satellite/themes/satellite_theme/templates/page/page.html.twig", "C:\\Users\\Karolina\\Desktop\\esn\\public_html\\web\\profiles\\contrib\\satellite\\themes\\satellite_theme\\templates\\page\\page.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("embed" => 49, "if" => 80);
        static $filters = array("escape" => 83, "nl2br" => 117);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['embed', 'if'],
                ['escape', 'nl2br'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}


/* profiles/contrib/satellite/themes/satellite_theme/templates/page/page.html.twig */
class __TwigTemplate_06444da3e72c530d03d3fd7c9af1dc0532c832edc7718f7d45130791a99fca98___225828807 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'branding' => [$this, 'block_branding'],
            'left' => [$this, 'block_left'],
            'right' => [$this, 'block_right'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doGetParent(array $context)
    {
        // line 49
        return "@radix/navbar/navbar.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@radix/navbar/navbar.twig", "profiles/contrib/satellite/themes/satellite_theme/templates/page/page.html.twig", 49);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 56
    public function block_branding($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 57
        echo "      ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "navbar_branding", [], "any", false, false, true, 57)) {
            // line 58
            echo "        ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "navbar_branding", [], "any", false, false, true, 58), 58, $this->source), "html", null, true);
            echo "
      ";
        }
        // line 60
        echo "    ";
    }

    // line 62
    public function block_left($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 63
        echo "      ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "navbar_left", [], "any", false, false, true, 63)) {
            // line 64
            echo "        <div class=\"mr-auto ms-auto mb-2 mb-lg-0\">
          ";
            // line 65
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "navbar_left", [], "any", false, false, true, 65), 65, $this->source), "html", null, true);
            echo "
        </div>
      ";
        }
        // line 68
        echo "    ";
    }

    // line 70
    public function block_right($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 71
        echo "      ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "navbar_right", [], "any", false, false, true, 71)) {
            // line 72
            echo "        ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "navbar_right", [], "any", false, false, true, 72), 72, $this->source), "html", null, true);
            echo "
      ";
        }
        // line 74
        echo "    ";
    }

    public function getTemplateName()
    {
        return "profiles/contrib/satellite/themes/satellite_theme/templates/page/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  318 => 74,  312 => 72,  309 => 71,  305 => 70,  301 => 68,  295 => 65,  292 => 64,  289 => 63,  285 => 62,  281 => 60,  275 => 58,  272 => 57,  268 => 56,  257 => 49,  173 => 143,  167 => 141,  164 => 140,  158 => 138,  155 => 137,  149 => 135,  147 => 134,  140 => 129,  132 => 127,  130 => 126,  119 => 119,  115 => 117,  111 => 116,  100 => 108,  96 => 107,  87 => 100,  81 => 95,  74 => 91,  70 => 89,  68 => 88,  65 => 87,  58 => 83,  54 => 81,  52 => 80,  49 => 79,  45 => 76,  43 => 50,  42 => 49,  39 => 48,);
    }

    public function getSourceContext()
    {
        return new Source("", "profiles/contrib/satellite/themes/satellite_theme/templates/page/page.html.twig", "C:\\Users\\Karolina\\Desktop\\esn\\public_html\\web\\profiles\\contrib\\satellite\\themes\\satellite_theme\\templates\\page\\page.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("if" => 57);
        static $filters = array("escape" => 58);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
