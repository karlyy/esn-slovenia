<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @radix/table/table.twig */
class __TwigTemplate_9de7240e4106dfc1202c8b31e648a40525997e255fd1b6f0c40bd46d091eed4e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'table' => [$this, 'block_table'],
            'caption' => [$this, 'block_caption'],
            'colgroup' => [$this, 'block_colgroup'],
            'header' => [$this, 'block_header'],
            'body' => [$this, 'block_body'],
            'footer' => [$this, 'block_footer'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 11
        echo "
";
        // line 12
        $this->displayBlock('table', $context, $blocks);
    }

    public function block_table($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "  ";
        $context["classes"] = twig_array_merge([0 => "table"], ((        // line 15
($context["utility_classes"] ?? null)) ? (($context["utility_classes"] ?? null)) : ([])));
        // line 16
        echo "
  <table";
        // line 17
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 17), 17, $this->source), "html", null, true);
        echo ">
    ";
        // line 18
        if (($context["caption"] ?? null)) {
            // line 19
            echo "      ";
            $this->displayBlock('caption', $context, $blocks);
            // line 22
            echo "    ";
        }
        // line 23
        echo "
    ";
        // line 24
        $this->displayBlock('colgroup', $context, $blocks);
        // line 37
        echo "
    ";
        // line 38
        $this->displayBlock('header', $context, $blocks);
        // line 56
        echo "
    ";
        // line 57
        $this->displayBlock('body', $context, $blocks);
        // line 83
        echo "
    ";
        // line 84
        $this->displayBlock('footer', $context, $blocks);
        // line 99
        echo "  </table>
";
    }

    // line 19
    public function block_caption($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 20
        echo "        <caption>";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["caption"] ?? null), 20, $this->source), "html", null, true);
        echo "</caption>
      ";
    }

    // line 24
    public function block_colgroup($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 25
        echo "      ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["colgroups"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["colgroup"]) {
            // line 26
            echo "        ";
            if (twig_get_attribute($this->env, $this->source, $context["colgroup"], "cols", [], "any", false, false, true, 26)) {
                // line 27
                echo "          <colgroup";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["colgroup"], "attributes", [], "any", false, false, true, 27), 27, $this->source), "html", null, true);
                echo ">
            ";
                // line 28
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["colgroup"], "cols", [], "any", false, false, true, 28));
                foreach ($context['_seq'] as $context["_key"] => $context["col"]) {
                    // line 29
                    echo "              <col";
                    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["col"], "attributes", [], "any", false, false, true, 29), 29, $this->source), "html", null, true);
                    echo " />
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['col'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 31
                echo "          </colgroup>
        ";
            } else {
                // line 33
                echo "          <colgroup";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["colgroup"], "attributes", [], "any", false, false, true, 33), 33, $this->source), "html", null, true);
                echo " />
        ";
            }
            // line 35
            echo "      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['colgroup'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "    ";
    }

    // line 38
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 39
        echo "      ";
        if (($context["header"] ?? null)) {
            // line 40
            echo "          <thead>
            <tr>
              ";
            // line 42
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["header"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["cell"]) {
                // line 43
                echo "                ";
                // line 44
                $context["cell_classes"] = [0 => ((twig_get_attribute($this->env, $this->source,                 // line 45
$context["cell"], "active_table_sort", [], "any", false, false, true, 45)) ? ("is-active") : (""))];
                // line 48
                echo "                <";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["cell"], "tag", [], "any", false, false, true, 48), 48, $this->source), "html", null, true);
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["cell"], "attributes", [], "any", false, false, true, 48), "addClass", [0 => ($context["cell_classes"] ?? null)], "method", false, false, true, 48), 48, $this->source), "html", null, true);
                echo ">";
                // line 49
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["cell"], "content", [], "any", false, false, true, 49), 49, $this->source), "html", null, true);
                // line 50
                echo "</";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["cell"], "tag", [], "any", false, false, true, 50), 50, $this->source), "html", null, true);
                echo ">
              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cell'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 52
            echo "            </tr>
          </thead>
      ";
        }
        // line 55
        echo "    ";
    }

    // line 57
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 58
        echo "      ";
        if (($context["rows"] ?? null)) {
            // line 59
            echo "        <tbody>
          ";
            // line 60
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["rows"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
                // line 61
                echo "            ";
                // line 62
                $context["row_classes"] = [0 => (( !                // line 63
($context["no_striping"] ?? null)) ? (twig_cycle([0 => "odd", 1 => "even"], $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["loop"], "index0", [], "any", false, false, true, 63), 63, $this->source))) : (""))];
                // line 66
                echo "          <tr";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["row"], "attributes", [], "any", false, false, true, 66), "addClass", [0 => ($context["row_classes"] ?? null)], "method", false, false, true, 66), 66, $this->source), "html", null, true);
                echo ">
            ";
                // line 67
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["row"], "cells", [], "any", false, false, true, 67));
                foreach ($context['_seq'] as $context["_key"] => $context["cell"]) {
                    // line 68
                    echo "              <";
                    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["cell"], "tag", [], "any", false, false, true, 68), 68, $this->source), "html", null, true);
                    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["cell"], "attributes", [], "any", false, false, true, 68), 68, $this->source), "html", null, true);
                    echo ">";
                    // line 69
                    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["cell"], "content", [], "any", false, false, true, 69), 69, $this->source), "html", null, true);
                    // line 70
                    echo "</";
                    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["cell"], "tag", [], "any", false, false, true, 70), 70, $this->source), "html", null, true);
                    echo ">
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cell'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 72
                echo "            </tr>
          ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 74
            echo "          </tbody>
        ";
        } elseif (        // line 75
($context["empty"] ?? null)) {
            // line 76
            echo "          <tbody>
          <tr class=\"odd\">
            <td colspan=\"";
            // line 78
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["header_columns"] ?? null), 78, $this->source), "html", null, true);
            echo "\" class=\"empty message\">";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["empty"] ?? null), 78, $this->source), "html", null, true);
            echo "</td>
          </tr>
        </tbody>
      ";
        }
        // line 82
        echo "    ";
    }

    // line 84
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 85
        echo "      ";
        if (($context["footer"] ?? null)) {
            // line 86
            echo "        <tfoot>
          ";
            // line 87
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["footer"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
                // line 88
                echo "          <tr";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["row"], "attributes", [], "any", false, false, true, 88), 88, $this->source), "html", null, true);
                echo ">
            ";
                // line 89
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["row"], "cells", [], "any", false, false, true, 89));
                foreach ($context['_seq'] as $context["_key"] => $context["cell"]) {
                    // line 90
                    echo "              <";
                    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["cell"], "tag", [], "any", false, false, true, 90), 90, $this->source), "html", null, true);
                    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["cell"], "attributes", [], "any", false, false, true, 90), 90, $this->source), "html", null, true);
                    echo ">";
                    // line 91
                    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["cell"], "content", [], "any", false, false, true, 91), 91, $this->source), "html", null, true);
                    // line 92
                    echo "</";
                    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["cell"], "tag", [], "any", false, false, true, 92), 92, $this->source), "html", null, true);
                    echo ">
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cell'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 94
                echo "            </tr>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 96
            echo "        </tfoot>
      ";
        }
        // line 98
        echo "    ";
    }

    public function getTemplateName()
    {
        return "@radix/table/table.twig";
    }

    public function getDebugInfo()
    {
        return array (  349 => 98,  345 => 96,  338 => 94,  329 => 92,  327 => 91,  322 => 90,  318 => 89,  313 => 88,  309 => 87,  306 => 86,  303 => 85,  299 => 84,  295 => 82,  286 => 78,  282 => 76,  280 => 75,  277 => 74,  262 => 72,  253 => 70,  251 => 69,  246 => 68,  242 => 67,  237 => 66,  235 => 63,  234 => 62,  232 => 61,  215 => 60,  212 => 59,  209 => 58,  205 => 57,  201 => 55,  196 => 52,  187 => 50,  185 => 49,  180 => 48,  178 => 45,  177 => 44,  175 => 43,  171 => 42,  167 => 40,  164 => 39,  160 => 38,  156 => 36,  150 => 35,  144 => 33,  140 => 31,  131 => 29,  127 => 28,  122 => 27,  119 => 26,  114 => 25,  110 => 24,  103 => 20,  99 => 19,  94 => 99,  92 => 84,  89 => 83,  87 => 57,  84 => 56,  82 => 38,  79 => 37,  77 => 24,  74 => 23,  71 => 22,  68 => 19,  66 => 18,  62 => 17,  59 => 16,  57 => 15,  55 => 13,  48 => 12,  45 => 11,);
    }

    public function getSourceContext()
    {
        return new Source("", "@radix/table/table.twig", "C:\\Users\\Karolina\\Desktop\\esn\\public_html\\web\\themes\\contrib\\radix\\src\\components\\table\\table.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("block" => 12, "set" => 13, "if" => 18, "for" => 25);
        static $filters = array("merge" => 15, "escape" => 17);
        static $functions = array("cycle" => 63);

        try {
            $this->sandbox->checkSecurity(
                ['block', 'set', 'if', 'for'],
                ['merge', 'escape'],
                ['cycle']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
