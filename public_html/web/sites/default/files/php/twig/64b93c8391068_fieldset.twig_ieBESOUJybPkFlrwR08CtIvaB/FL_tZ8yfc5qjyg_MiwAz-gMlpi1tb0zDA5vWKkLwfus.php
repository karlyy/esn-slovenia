<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @radix/form/fieldset.twig */
class __TwigTemplate_4b65864f90819b4937dad53132caae4a4e5f7699bd44f8c412c41e246b299e3e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        $context["classes"] = [0 => "js-form-item", 1 => "form-item", 2 => "js-form-wrapper", 3 => "form-wrapper", 4 => "mb-3"];
        // line 32
        echo "<fieldset";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 32), 32, $this->source), "html", null, true);
        echo ">
  ";
        // line 34
        $context["legend_classes"] = [0 => "fieldset-legend", 1 => "m-0", 2 => ((        // line 37
($context["required"] ?? null)) ? ("js-form-required") : ("")), 3 => ((        // line 38
($context["required"] ?? null)) ? ("form-required") : (""))];
        // line 41
        echo "  
  ";
        // line 42
        if ((($context["title_display"] ?? null) != "none")) {
            // line 43
            echo "    <legend";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["legend"] ?? null), "attributes", [], "any", false, false, true, 43), "addClass", [0 => ($context["legend_classes"] ?? null)], "method", false, false, true, 43), 43, $this->source), "html", null, true);
            echo ">
      ";
            // line 44
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["legend"] ?? null), "title", [], "any", false, false, true, 44), 44, $this->source), "html", null, true);
            // line 45
            if ((($context["required"] ?? null) && (($context["title_display"] ?? null) == "before"))) {
                // line 46
                echo "<span class=\"font-weight-bolder form-required--indicator ps-1 text-danger\">*</span>";
            }
            // line 48
            echo "</legend>
  ";
        }
        // line 50
        echo "
  <div class=\"fieldset-wrapper\">
    ";
        // line 52
        if (((($context["description_display"] ?? null) == "before") && twig_get_attribute($this->env, $this->source, ($context["description"] ?? null), "content", [], "any", false, false, true, 52))) {
            // line 53
            echo "      <small";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["description"] ?? null), "attributes", [], "any", false, false, true, 53), "addClass", [0 => "description", 1 => "form-text", 2 => "text-muted"], "method", false, false, true, 53), 53, $this->source), "html", null, true);
            echo ">";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["description"] ?? null), "content", [], "any", false, false, true, 53), 53, $this->source), "html", null, true);
            echo "</small>
    ";
        }
        // line 55
        echo "    ";
        if (($context["errors"] ?? null)) {
            // line 56
            echo "      <div>
        ";
            // line 57
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["errors"] ?? null), 57, $this->source), "html", null, true);
            echo "
      </div>
    ";
        }
        // line 60
        echo "    ";
        if (($context["prefix"] ?? null)) {
            // line 61
            echo "      <span class=\"field-prefix\">";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["prefix"] ?? null), 61, $this->source), "html", null, true);
            echo "</span>
    ";
        }
        // line 63
        echo "    ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["children"] ?? null), 63, $this->source), "html", null, true);
        echo "
    ";
        // line 64
        if (($context["suffix"] ?? null)) {
            // line 65
            echo "      <span class=\"field-suffix\">";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["suffix"] ?? null), 65, $this->source), "html", null, true);
            echo "</span>
    ";
        }
        // line 67
        echo "    ";
        if ((twig_in_filter(($context["description_display"] ?? null), [0 => "after", 1 => "invisible"]) && twig_get_attribute($this->env, $this->source, ($context["description"] ?? null), "content", [], "any", false, false, true, 67))) {
            // line 68
            echo "      <small";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["description"] ?? null), "attributes", [], "any", false, false, true, 68), "addClass", [0 => "description", 1 => "form-text", 2 => "text-muted"], "method", false, false, true, 68), 68, $this->source), "html", null, true);
            echo ">";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["description"] ?? null), "content", [], "any", false, false, true, 68), 68, $this->source), "html", null, true);
            echo "</small>
    ";
        }
        // line 70
        echo "  </div>
</fieldset>
";
    }

    public function getTemplateName()
    {
        return "@radix/form/fieldset.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 70,  122 => 68,  119 => 67,  113 => 65,  111 => 64,  106 => 63,  100 => 61,  97 => 60,  91 => 57,  88 => 56,  85 => 55,  77 => 53,  75 => 52,  71 => 50,  67 => 48,  64 => 46,  62 => 45,  60 => 44,  55 => 43,  53 => 42,  50 => 41,  48 => 38,  47 => 37,  46 => 34,  41 => 32,  39 => 24,);
    }

    public function getSourceContext()
    {
        return new Source("", "@radix/form/fieldset.twig", "C:\\Users\\Karolina\\Desktop\\esn\\public_html\\web\\themes\\contrib\\radix\\src\\components\\form\\fieldset.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 24, "if" => 42);
        static $filters = array("escape" => 32);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
